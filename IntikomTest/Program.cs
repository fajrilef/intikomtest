﻿using System;

namespace IntikomTest
{
    class Program
    {
        static string[] satuan = new string[] { "", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan" };
        static string[] puluhBelas = new string[] { "puluh", "belas" };
        static string[] ribuJutaMiliar = { "", " Ribu", " Juta", " Miliar" };

        private static string NamingInt(int n, string digit, int ribuJuta)
        {
            if (n == 0)
            {
                return digit;
            }

            string result = digit;

            if (result.Length > 0)
            {
                result += " ";
            }

            if (n < 10)
            {
                if(ribuJuta == 1 && n == 1)
                {
                    result += "Se";
                }
                else
                {
                    result += satuan[n];
                }
               
            }
            else if (n < 20)
            {
                if (n < 12)
                {
                    result += "se" + puluhBelas[n - 10];
                }
                else
                {
                    result += satuan[n - 10] + " Belas";
                }
            }
            else if (n < 100)
            {
                string firstNumber = n.ToString().Substring(0, 1);
                string lastNumber = n.ToString().Substring(1);
                result += satuan[Convert.ToInt32(firstNumber)] + " Puluh ";
                result += NamingInt(Convert.ToInt32(lastNumber), "", 0);
            }
            else if (n < 1000)
            {
                string firstNumber = n.ToString().Substring(0, 1);
                string lastNumber = n.ToString().Substring(1);
                if(firstNumber == "1")
                {
                    result += "Seratus ";
                }
                else
                {
                    result += satuan[Convert.ToInt32(firstNumber)] + " Ratus ";
                }
                result += NamingInt(Convert.ToInt32(lastNumber), "", 0);
            }
            else
            {
                result += NamingInt(n % 1000, NamingInt(n / 1000, "", ribuJuta + 1), 0);
                if (n % 1000 == 0)
                {
                    return result;
                }
            }

            return result + ribuJutaMiliar[ribuJuta];
        }

        public static string ConvertIntToString(int n)
        {
            if (n == 0)
            {
                return "Zero";
            }
            else if (n < 0)
            {
                return "Negative " + ConvertIntToString(-n);
            }

            return NamingInt(n, "", 0);
        }

        static void Main(string[] args)
        {
            Console.Write("Masukkan Angka :");
            try
            {
                int angka = Convert.ToInt32(Console.ReadLine());
                string hasil = ConvertIntToString(angka);
                Console.WriteLine(hasil);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
    }
}
